package com.app.springApplication.repository;

import com.google.gson.JsonObject;
import org.springframework.stereotype.Repository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@Repository
public class PaymentRepository {

    private static final String LOGIN_URL = "https://sandbox-reporting.rpdpymnt.com/api/v3/merchant/user/login";
    private static final String GET_TRANSACTION_URL = "https://sandbox-reporting.rpdpymnt.com/api/v3/transaction";
    private static final String GET_CUSTOMER_URL = "https://sandbox-reporting.rpdpymnt.com/api/v3/client";
    private static final String GET_TRANSACTION_LIST_URL = "https://sandbox-reporting.rpdpymnt.com/api/v3/transaction/list";

    public String getToken(String userName, String password) throws IOException {
        HttpURLConnection conn = getHttpURLConnection(LOGIN_URL, "POST");
        JsonObject loginJson = getLoginJson(userName, password);
        return sendRequestToEndPoint(conn, loginJson);
    }

    public String getTransactionInfo(String transactionId, String token) throws IOException {
        HttpURLConnection conn = getHttpURLConnection(GET_TRANSACTION_URL, "GET");
        conn.setRequestProperty("Authorization", token);
        JsonObject jsonObject = getTransactionJson(transactionId);
        return sendRequestToEndPoint(conn, jsonObject);
    }

    public String getCustomerInfo(String transactionId, String token) throws IOException {
        HttpURLConnection conn = getHttpURLConnection(GET_CUSTOMER_URL, "GET");
        conn.setRequestProperty("Authorization", token);
        JsonObject jsonObject = getTransactionJson(transactionId);
        return sendRequestToEndPoint(conn, jsonObject);
    }

    public String getTransactionList(String token) throws IOException {
        HttpURLConnection conn = getHttpURLConnection(GET_TRANSACTION_LIST_URL, "GET");
        conn.setRequestProperty("Authorization", token);
        String fromDate = "2005-07-01";
        String toDate = "2021-10-01";
        JsonObject jsonObject = getJsonObjectWithDateParameters(fromDate, toDate);
        return sendRequestToEndPoint(conn, jsonObject);
    }

    private HttpURLConnection getHttpURLConnection(String urlStr, String requestMethod) throws IOException {
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        conn.setRequestMethod(requestMethod);
        conn.setDoOutput(true);
        return conn;
    }

    private JsonObject getLoginJson(String email, String password) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("email", email);
        jsonObject.addProperty("password", password);
        return jsonObject;
    }

    private JsonObject getTransactionJson(String transactionId) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("transactionId", transactionId);
        return jsonObject;
    }

    private JsonObject getJsonObjectWithDateParameters(String fromDate, String toDate) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("fromDate", fromDate);
        jsonObject.addProperty("toDate", toDate);
        return jsonObject;
    }

    private String sendRequestToEndPoint(HttpURLConnection conn, JsonObject jsonObject) throws IOException {
        OutputStream os = conn.getOutputStream();
        os.write(jsonObject.toString().getBytes());
        os.flush();

        if (conn.getResponseCode() != 200) {
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getErrorStream())));

            StringBuilder sb = new StringBuilder();
            String currentLine;
            while ((currentLine = br.readLine()) != null) {
                sb.append(currentLine);
            }
            throw new RuntimeException("Failed : HTTP error code : "
                    + conn.getResponseCode() + sb);
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (conn.getInputStream())));

        StringBuilder sb = new StringBuilder();
        String currentLine;
        while ((currentLine = br.readLine()) != null) {
            sb.append(currentLine);
        }

        conn.disconnect();
        return sb.toString();
    }

}
