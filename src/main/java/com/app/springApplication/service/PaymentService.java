package com.app.springApplication.service;

import com.app.springApplication.repository.PaymentRepository;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
public class PaymentService {
    private final PaymentRepository paymentRepository;

    public PaymentService(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    public String getToken(String userName, String password) throws IOException {
        String responseStr = paymentRepository.getToken(userName, password);
        JsonElement resultJson = JsonParser.parseString(responseStr);

        return resultJson.getAsJsonObject().get("token").getAsString();
    }

    public String getTransactionInfo(String transactionId, String token) throws IOException {
        String responseStr = paymentRepository.getTransactionInfo(transactionId, token);
        return JsonParser.parseString(responseStr).getAsJsonObject().get("transaction").toString();
    }

    public String getCustomerInfo(String transactionId, String token) throws IOException {
        String responseStr = paymentRepository.getCustomerInfo(transactionId, token);
        return JsonParser.parseString(responseStr).getAsJsonObject().get("customerInfo").toString();
    }

    public String getTransactionList(String token) throws IOException {
        String responseStr = paymentRepository.getTransactionList(token);
        return formatResponse(responseStr);
    }

    private static String formatResponse(String responseStr) {
        JsonArray jsonArray = ((JsonObject) JsonParser.parseString(responseStr)).get("data").getAsJsonArray();
        Stream<JsonElement> stream = StreamSupport.stream(jsonArray.spliterator(), true);
        JsonArray transactionList = stream.map(k -> k.getAsJsonObject().get("transaction").getAsJsonObject().get("merchant")).collect(JsonArray::new,
                JsonArray::add,
                JsonArray::addAll);
        JsonObject responseJson = new JsonObject();
        responseJson.add("data", transactionList);
        return responseJson.toString();
    }
}
