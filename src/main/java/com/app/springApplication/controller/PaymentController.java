package com.app.springApplication.controller;

import com.app.springApplication.service.PaymentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
@RequestMapping("/")
public class PaymentController {
    private final PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @GetMapping("/")
    public String login() {
        return "home";
    }

    @PostMapping(value = "/getToken")
    public @ResponseBody
    String getToken(HttpServletRequest request, @RequestParam("userName") String userName, @RequestParam("password") String password) throws IOException {
        String token = paymentService.getToken(userName, password);
        request.getSession().setAttribute("token", token);
        return "";
    }

    @GetMapping("/getTransactionInfo")
    public @ResponseBody
    String getTransactionInfo(HttpServletRequest request, @RequestParam("transactionId") String transactionId) throws IOException {
        String token = String.valueOf(request.getSession().getAttribute("token"));
        return paymentService.getTransactionInfo(transactionId, token);
    }

    @GetMapping("/getCustomerInfo")
    public @ResponseBody
    String getCustomerInfo(HttpServletRequest request, @RequestParam("transactionId") String transactionId) throws IOException {
        String token = String.valueOf(request.getSession().getAttribute("token"));
        return paymentService.getCustomerInfo(transactionId, token);
    }

    @GetMapping("/getTransactionList")
    public @ResponseBody
    String getTransactionList(HttpServletRequest request) throws IOException {
        String token = String.valueOf(request.getSession().getAttribute("token"));
        return paymentService.getTransactionList(token);
    }
}
