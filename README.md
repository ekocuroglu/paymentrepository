* This is a Springboot web application for payment endpoints. The source code url is "https://bitbucket.org/ekocuroglu/paymentrepository/src/master/".

* I have used 4 enpoints from given document.

* When application runs, first login screen appears. After entering credentials, login is completed. For this process, I have used "​https://sandbox-reporting.rpdpymnt.com/api/v3/merchant/user/login" endpoint.

* After login, a bootstrap data table appears which is used to show transaction list. For this part(to fill data table with transaction list) I have used "https://sandbox-reporting.rpdpymnt.com/api/v3/transaction/list" endpoint.

* In the table there are some columns about transaction information and an additional column which contains 2 buttons to show transaction and customer information.

* When we click the transaction button, it will send an ajax call to backend to get transaction information. After getting response, an alert box will pop up to show server's response. For that part I have used "​https://sandbox-reporting.rpdpymnt.com/api/v3/transaction" endpoint.

* When we click the customer button, it will send an ajax call to backend to get customer information. After getting response, an alert box will pop up to show server's response. For that part I have used "​​​https://sandbox-reporting.rpdpymnt.com/api/v3/client" endpoint.

* I have used some Java 8 features to convert API's response to appropriate format.

* I have written unit tests to check conversion between APIs.







